def check_exp_is_valid(expression: str):
    oposites = {
      ')': '(',
      ']': '[',
      '}': '{',
    }
    stack = []
    for i in range(len(expression)):
        if expression[i] in ['(', '[', '{']:
            stack.append(expression[i])
        elif expression[i] in [')', ']', '}']:
            if len(stack) > 0 and stack[-1] == oposites[expression[i]]:
                stack.pop()
            else:
                return False
    return stack == []



assert True == check_exp_is_valid('()')
assert True == check_exp_is_valid('(())')
assert True == check_exp_is_valid('([])')
assert True == check_exp_is_valid('[]()')
assert True == check_exp_is_valid('[]({})')
assert True == check_exp_is_valid('[{}](){}')
assert False == check_exp_is_valid('[()')
assert False == check_exp_is_valid(')(')
assert False == check_exp_is_valid(')')
assert False == check_exp_is_valid('(')
assert False == check_exp_is_valid('(]')
assert False == check_exp_is_valid('()]')
assert False == check_exp_is_valid('([')
assert False == check_exp_is_valid('[')
assert False == check_exp_is_valid(']')
assert False == check_exp_is_valid('](')
assert False == check_exp_is_valid('{](')
assert False == check_exp_is_valid('(){')
assert False == check_exp_is_valid('()}')
