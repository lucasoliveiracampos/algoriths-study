import java.util.Queue;
import java.util.List;
import java.util.LinkedList;

/**
Started exercise at 13:45 completed at 14:11
output:
100, 
50, 200, 
300, 
350

Gonna print the sibling node now, just to be sure it's working fine
Started at: 14:14  completed at 14:20
*/
public class LevelOrderTraversal {

    public static class Node {
        int value;
        Node left;
        Node right;
        Node sibling;

        public Node(int value) {
            this.value = value;
        }

    }

    public static Node connectAllSiblings(Node node) {
        Queue<Node> mainQueue = new LinkedList<Node>();
        mainQueue.add(node);
        while (mainQueue.size() > 0) {
            Node currentNode = mainQueue.remove();
            System.out.print(String.format("%s, ", currentNode.value));
            Queue<Node> branchesQueue = new LinkedList<Node>();
            if (currentNode.left != null) {
                branchesQueue.add(currentNode.left);
            }
            if (currentNode.right != null) {
                branchesQueue.add(currentNode.right);
            }
            if (branchesQueue.size() > 0) {
                if (mainQueue.size() > 0) {
                    currentNode.sibling = mainQueue.element();
                } else {
                    currentNode.sibling = branchesQueue.element();
                }
            }
            if (mainQueue.size() == 0) {
                System.out.println("");
                mainQueue = branchesQueue;
            }
        }

        return node;
    }

    public static void printAllSiblings(Node node) {
        System.out.print(String.format("%s ,", node.value));
        if (node.sibling != null) {
            printAllSiblings(node.sibling);
        }
    }
    
    public static void main(String[] args) {
        Node node100 = new Node(100);
        Node node50 = new Node(50);
        Node node200 = new Node(200);
        Node node25 = new Node(25);
        Node node75 = new Node(75);
        Node node300 = new Node(300);
        Node node350 = new Node(350);

        node100.left = node50;
        node100.right = node200;
        node50.left = node25;
        node50.right = node75;
        node200.right = node300;
        node300.right = node350;

        node100 = connectAllSiblings(node100);

        System.out.println("COMPLETED SETUP FROM SIBLINGS!");

        printAllSiblings(node100);
        System.out.println("");
    }
}
