# Connect All Siblings
## Problem Statement

Given the root to a binary tree where each node has an additional pointer called sibling (or next), connect the sibling pointer to the next node in the same level. The last node in each level should point to the first node of the next level in the tree.

Consider the following binary tree.

        100
         /\
       50  200
       /\   |
     25  75 |
           300
            |
           350
            |
           NULL

Here’s how the final tree would look when all sibling pointers have been connected.

       -----100
       |    /\
       -->50->200 -> going to 25
          /\   |
200to-> 25  75 |
            |->300-|
                |  |
               350<'
                |
               NULL

## Hint
- Level order traversal
