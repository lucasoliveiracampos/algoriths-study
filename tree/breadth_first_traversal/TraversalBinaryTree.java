import java.util.List;
import java.util.Queue;
import java.util.LinkedList;
//started at 23:35 ... completed tree logic but got stuck at 00:09 to print the tree per level.
//around 00:24 I tried another approach based on print by level and just that level...
//It seems a workaround... but at least it should do the job. There is space for improvement!
public class TraversalBinaryTree {

    public static class Node {
        int value;
        Node leftNode;
        Node rightNode;

        public Node(int value) {
            this.value = value;
        }

        public Node(int value, Node leftNode, Node rightNode) {
            this.value = value;
            this.leftNode = leftNode;
            this.rightNode = rightNode;
        }
    }

    public static class Tree {
        Node node;

        private Node add(Node root, Node node) {
            if (root == null) {
                root = node;
                System.out.println(String.format("Added value %s", node.value));
            } else if (node.value < root.value) {
                root.leftNode = add(root.leftNode, node);
            } else if (node.value > root.value) {
                root.rightNode = add(root.rightNode, node);
            }
            return root;
        }

        public void add(int value) {
            if (this.node == null){
                this.node = new Node(value);
            } else {
                this.node = add(this.node, new Node(value));
            }
        }

        /**
            First solution... not optimal as we're visiting each node more than once...
        */
        public void printTreePerLevel(int level) {
            int currentLevel = 1;
            if (this.node != null) {
                if (currentLevel == level){
                    System.out.println(this.node.value);
                    return;
                } else {
                    printSides(this.node.leftNode, this.node.rightNode, ++currentLevel, level);
                }
                System.out.println("");
            } else {
                System.out.println("UNEXPECTED NULL");
            }
        }

        private void printSides(Node leftNode, Node rightNode, int currentLevel, int level) {
            if (currentLevel ==  level){
                if (leftNode != null) {
                    System.out.print(String.format("%s, ", leftNode.value));
                }
                if (rightNode != null) {
                    System.out.print(String.format("%s, ", rightNode.value));
                }
                return;
            }
            currentLevel++;
            if (leftNode != null) {
                printSides(leftNode.leftNode, leftNode.rightNode, currentLevel, level);
            }
            if (rightNode != null) {
                printSides(rightNode.leftNode, rightNode.rightNode, currentLevel, level);
            }
        }

        /**
            Second solution... optimal one... started at 13:20... completed concept at 13:40
            There was still an issue with the print, but the logic itself is correct.
            Completed to fix printing...
        */
        private void printTreePerLevelWithPrintError() {
            if (this.node != null) {
                Queue<Node> q = new LinkedList<Node>();
                Queue<Node> q2 = new LinkedList<Node>();
                q.add(this.node);
                System.out.print(this.node.value);
                while (q.size() > 0 || q2.size() > 0) {
                    Node node = q.remove();
                    if (node.leftNode != null) {
                        q.add(node.leftNode);
                        System.out.print(String.format("%s, ", node.leftNode.value));
                        q2.add(node.leftNode);
                    }
                    if (node.rightNode != null) {
                        q.add(node.rightNode);
                        System.out.print(String.format("%s, ", node.rightNode.value));
                        q2.add(node.rightNode);
                    }
                }
            }
        }

        /**
            After a long time, checking BFS examples, had an idea...
            Started to implement at 15:20... 15:38 completed RIGHT SOLUTION!
            ALELUIA JESUUUS!
        */
        private void printTreePerLevel() {
            if (this.node != null) {
                Queue<Queue<Node>> listQueue = new LinkedList<Queue<Node>>();
                Queue<Node> q = new LinkedList<Node>();
                q.add(this.node);
                listQueue.add(q);
                System.out.print(this.node.value);
                while (listQueue.size() > 0) {
                    q = listQueue.remove();
                    Queue<Node> newQueue = new LinkedList<Node>();
                    System.out.println("");
                    while (q.size() > 0) {
                        Node node = q.remove();
                        if (node.leftNode != null) {
                            System.out.print(String.format("%s, ", node.leftNode.value));
                            newQueue.add(node.leftNode);
                        }
                        if (node.rightNode != null) {
                            System.out.print(String.format("%s, ", node.rightNode.value));
                            newQueue.add(node.rightNode);
                        }
                    }
                    if (newQueue.size() > 0) {
                        listQueue.add(newQueue);
                    }
                }
            }
        }
    }

    public static void main(String[] args){
        Tree tree = new Tree();
        tree.add(100);
        tree.add(50);
        tree.add(25);
        tree.add(75);
        tree.add(200);
        tree.add(350);

        tree.printTreePerLevel(1);
        tree.printTreePerLevel(2);
        tree.printTreePerLevel(3);
        System.out.println("===== FINISHED SOLUTION 1 =====");

        tree.printTreePerLevel();
        System.out.println("===== FINISHED OPTIMAL SOLUTION =====");

    }

}