//started to create binarySearch at 13:16 and finished at 13:31 (good time!)
public class BinarySearch {

    static int[] array = {-120, -30, -10, -2, 1, 4, 15, 17, 101, 395};

    public static int binarySearch(int value, int ini, int last) {
        int mid = -1;
        try {
            if (last == ini) return -1;
            
            mid = ((last - ini) / 2) + ini;
            if (array[mid] == value) return mid;
            if (array[mid] > value) return binarySearch(value, ini, mid);
            if (array[mid] < value) return binarySearch(value, mid+1, last);
        } catch (Exception e) {
            System.out.println(String.format("ERROR mid = %s", mid));
        }
        return -1;
    }

    public static void main(String[] args) {
        System.out.println(binarySearch(-10, 0, array.length));
        System.out.println(binarySearch(-120, 0, array.length));
        System.out.println(binarySearch(395, 0, array.length));
        System.out.println(binarySearch(396, 0, array.length));
        System.out.println(binarySearch(121, 0, array.length));
    }
}