# Make Columns and Rows Zero

## Problem Statement

Given a two-dimensional array, if any element within is zero, make its whole row and column zero. For example, consider the matrix below.

## Reference
https://www.educative.io/m/make-columns-and-rows-zero