import java.util.Random;
import java.util.List;
import java.util.HashSet;
import java.util.ArrayList;

// started at 22:09... completed idea around 22:35
/**
    started to add dynamic programming at 23:11 and completed the benchmark at 23:27.
    Weirdly the time to process was much higher using dynamic programming.
    e.g terminal NOT USING Dynamic Programming 
    ➜  make_columns_and_rows_zero git:(main) ✗ time java MakeColumnsAndRowsZero 
    1075098112
    java MakeColumnsAndRowsZero  0.70s user 0.02s system 107% cpu 0.666 total
    ➜  make_columns_and_rows_zero git:(main) ✗ time java MakeColumnsAndRowsZero
    1075098112
    java MakeColumnsAndRowsZero  0.71s user 0.02s system 108% cpu 0.671 total
    ➜  make_columns_and_rows_zero git:(main) ✗ time java MakeColumnsAndRowsZero
    1075098112
    java MakeColumnsAndRowsZero  0.68s user 0.04s system 107% cpu 0.666 total

    ➜  make_columns_and_rows_zero git:(main) ✗ time java MakeColumnsAndRowsZero 
    986040
    java MakeColumnsAndRowsZero  10.06s user 0.03s system 100% cpu 10.063 total
    ➜  make_columns_and_rows_zero git:(main) ✗ time java MakeColumnsAndRowsZero
    986040
    java MakeColumnsAndRowsZero  5.24s user 0.07s system 101% cpu 5.252 total
    ➜  make_columns_and_rows_zero git:(main) ✗ time java MakeColumnsAndRowsZero
    986040
    java MakeColumnsAndRowsZero  5.32s user 0.04s system 101% cpu 5.298 total

    It means, DYNAMIC PROGRAMMING IS ONLY USEFUL FOR COST OPERATIONAL CACHING!!
    Otherwise just set the value.

*/

public class MakeColumnsAndRowsZero {

    static int ROWS = 1000;
    static int COLUMNS = 1000;

    static class Point {
        int x;
        int y;

        public Point(int x, int y){
            this.x = x;
            this.y = y;
        }

    }

    static class Solution {
        List<Point> indexesToZero = new ArrayList<Point>();

        public void printMatrix(int[][] matrix) {
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[i].length; j++) {
                    System.out.print(String.format("%s,", matrix[i][j]));
                }
                System.out.println("");
            }
        }

        public void makeColumnsAndRowsZero(int[][] matrix) {
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[i].length; j++) {
                    if(matrix[i][j] == 0) {
                        indexesToZero.add(new Point(i, j));
                    }
                }
            }

            for(Point point: indexesToZero) {
                for (int i = 0; i < matrix.length; i++) {
                    matrix[i][point.y] = 0;
                    for (int j = 0; j < matrix[i].length; j++) {
                        matrix[point.x][j] = 0;
                    }
                }
            }

            printMatrix(matrix);
        }
    }


    static int[][] create_random_matrix(int rows, int cols) {
        int[][] v = new int[rows][cols];

        for(int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                int t = new Random().nextInt() % 100;
                v[i][j] = t + 1;
                if (Math.abs(t) % 100 <= 5) {
                    v[i][j] = 0;
                }
            }
        }
        return v;
    }

    static int positive(int value) {
        if (value < 0){
            return value * -1;
        } else {
            return value;
        }
    }

    static int[][] create_benchmark(int rows, int cols) {
        int[][] v = new int[rows][cols];

        for(int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                if (i % 100 == 0 || j % 250 == 0) {
                    v[i][j] = 0;
                } else {
                    v[i][j] = i + j + 1;
                }
            }
        }
        return v;
    }

    public static void main(String[] args) {

        //int[][] matrix = create_benchmark(ROWS, COLUMNS);

        int[][] matrix = {
            {5,4,3,9},
            {2,0,7,6},
            {1,3,4,0},
            {9,8,3,4}
        };

        Solution solution = new Solution();
        solution.makeColumnsAndRowsZero(matrix);
    }

}

