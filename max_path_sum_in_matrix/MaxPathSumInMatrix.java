import java.util.*;

public class MaxPathSumInMatrix {
    //started to write the code at ~11:40;
    //12:16 not best solution done;

    static int[][] visited = new int[3][5];

    static int counter = 0;

    private static int max(int a, int b) {
        return (a > b) ? a : b;
    }

    /**
    optimal solution, but consuming too much memory. as bigO is around (row ** 3)... 
    It should use dynamic Programing by somehow...
    Started to add Dynamic Programming in it at 15:18 finished at 15:30
    3rd solution was the best one using Dynamic Programming
    */
    private static int maxPathSum(int[][] matrix, int row, int col) {
        if (visited[row][col] < matrix[row][col]) {
            visited[row][col] = matrix[row][col];
        } else {
            return visited[row][col];
        }

        counter++;

        System.out.println("row: " + row);
        if (row == matrix.length - 1) {
            return matrix[row][col];
        }

        int nextCol = (col + 1 < matrix[row].length) ? col + 1 : col;
        int prevCol = (col - 1 > 0) ? col - 1 : col;

        System.out.println("row: " + row + " nextCol: " + nextCol + " prevCol: " + prevCol);

        int ahead = maxPathSumSecondSolution(matrix, row + 1, col);
        int right = maxPathSumSecondSolution(matrix, row + 1, nextCol);
        int left = maxPathSumSecondSolution(matrix, row + 1, prevCol);

        
        return matrix[row][col] + max(ahead, max(left, right));
    }

    /**
    Given a matrix of N * M. Find the maximum path sum in matrix. The maximum path is sum of all elements from first row to last row where you are allowed to move only down or diagonally to left or right where you start from a initial position.

    Input : mat[6][4] = 10 10  2  0 20  4
                         1  0  0 30  2  5
                         0 10  4  0  2  0
                         1  0  2 20  0  4
    Output : 74
    The maximum sum path is 20-30-4-20.

    Input : mat[3][3] = 1 2 3
                        9 8 7
                        4 5 6
    Output : 17
    The maximum sum path is 3-8-6.

    Start point: 4 (last column)
    Input : mat[5][3] = 1 1 1 1 1
                        1 1 1 1 2
                        2 2 9 1 1
    Output : 11    
    The maximum sum path is 1-1-9
    */
    public static void main(String[] args) {
        // Scanner scan = new Scanner(System.in);
        // int a = scan.nextInt();
        int N = 3;
        int M = 5;

        int[][] matrix = {
            {1, 1, 1, 1, 1},
            {1, 1, 1, 1, 2},
            {2, 2, 9, 1, 1}
        };

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                visited[i][j] = -1;
            }
        }


        System.out.println(maxPathSumSecondSolution(matrix, 0, 4));
        System.out.println(counter);
    }
}