def is_sum_of_two_values_in_array(array, expected_value):
    routed_numbers = set()
    for el in array:
        if expected_value - el in routed_numbers:
            return True
        routed_numbers.add(el)

    return False


array = [5, 7, 1, 2, 8, 4, 3]
for el in [10, 7, 3, 12, 9, 5]:
    if not is_sum_of_two_values_in_array(array, el):
        print(f"False, unexpected for {el}")

for el in [20, 16, 2, 14]:
    if is_sum_of_two_values_in_array(array, el):
        print(f"True, unexpected for {el}")

