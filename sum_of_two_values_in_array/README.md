# Sum of two values in array

## Problem Statement
Given an array of integers and a value, determine if there are any two integers in the array whose sum is equal to the given value. Return true if the sum exists and return false if it does not.

Consider this array and the target sums:
[5,7,1,2,8,4,3]

Target Sum:
10 = is True (7 + 3 = 10, 2 + 8 = 10)
19 = is False (No 2 values sum upto 19)

Hints
    Use hashing
    Use comparison between elements


## Reference
https://www.educative.io/m/sum-of-two-values