#around 30 min to do the solution... 
#still not understanding well the concept about "Handle carry" problems...
def add_two_integers(list1, list2):
    list1_len = len(list1)
    list2_len = len(list2)
    max_len = max(list1_len, list2_len)
    n = 0
    remaining = 0
    result = []
    while n < max_len:
        dig1 = 0 if n == list1_len else list1[n]
        dig2 = 0 if n == list2_len else list2[n]
        mod = int((dig1 + dig2 + remaining) % 10)
        remaining = int((dig1 + dig2 + remaining) / 10)
        n += 1
        result.append(mod)max_len
    if remaining:
        result.append(remaining)
    return result

assert [8, 3, 1, 0, 1] == add_two_integers([1,0,9,9], [7,3,2])
print(add_two_integers([1,0,9,9], [7,3,2,9]))