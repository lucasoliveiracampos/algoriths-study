# Add two integers

## Problem Statement

Given the head pointers of two linked lists where each linked list represents an integer number (each node is a digit), add them and return the resulting linked list. Here, the first node in a list represents the least significant digit.

head1 -> 1 -> 0 -> 9 -> 9 -> null
head2 -> 7 -> 3 -> 2 -> null
result-> 8 -> 3 -> 1 -> 0 -> 1 -> null

## Hint
- Handle carry (??? tafuck is it?!)