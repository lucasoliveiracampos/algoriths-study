import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.LinkedList;

// I've spent around 1 hours to create this code since the beginning.
// next challenge is DO NOT USE LIST!
public class ArbitraryPointer {

    static class LinkedListNode {
        int value;
        LinkedListNode arbitraryPointer;

        public LinkedListNode(int value, LinkedListNode arbitraryPointer) {
            this.value = value;
            this.arbitraryPointer = arbitraryPointer;
        }

        public LinkedListNode(int value) {
            this.value = value;
            this.arbitraryPointer = null;
        }

        public LinkedListNode setArbitraryPointer(LinkedListNode arbitraryPointer) {
            this.arbitraryPointer = arbitraryPointer;
            return this;
        }

        public String toString() {
            if (this.arbitraryPointer != null){
                return String.format("Current Value is %s reference value is %s", this.value, this.arbitraryPointer.value);
            } else {
                return String.format("Current Value is %s reference value is NULL", this.value);
            }
        }
    }

    public static List<LinkedListNode> deepCopy(final List<LinkedListNode> list) {
        LinkedList<LinkedListNode> newList = new LinkedList<>();
        Map<LinkedListNode, LinkedListNode> map = new HashMap<>();

        for (LinkedListNode el : list) {
            LinkedListNode newEl = new LinkedListNode(el.value);
            map.put(el, newEl);
            newList.add(newEl);
        }

        for (LinkedListNode el : list) {
            LinkedListNode lln = map.get(el);
            lln.setArbitraryPointer(map.get(el.arbitraryPointer));
        }

        return newList;
    }

    public static void printValues(final List<LinkedListNode> list) {
        for (LinkedListNode el : list) {
            System.out.println(el);
        }
    }

    public static void main(String[] args) {
        List<LinkedListNode> list = new LinkedList<>();
        LinkedListNode node1 = new LinkedListNode(7); 
        LinkedListNode node2 = new LinkedListNode(14);
        LinkedListNode node3 = new LinkedListNode(21, node1);

        list.add(node1);
        list.add(node2);
        list.add(node3);

        System.out.println("PRINTING INITIAL VALUES");
        printValues(list);

        List<LinkedListNode> newList = deepCopy(list);
        System.out.println("DEEP COPY DONE!");

        System.out.println("STARTING TO CHANGE THE VALUES");
        node1.value = 12;
        System.out.println(node3);

        System.out.println("VALUES FROM COPIED LIST");
        printValues(newList);

    }
}