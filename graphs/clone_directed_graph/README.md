# Clone a Directed Graph
## Problem Statement

Given the root node of a directed graph, clone this graph by creating its deep copy so that the cloned graph has the same vertices and edges as the original graph.

Let’s look at the below graphs as an example. If the input graph is G = (V, E) where V is set of vertices and E is set of edges, then the output graph (cloned graph) G’ = (V’, E’) such that V = V’ and E = E’.

>    Note: We are assuming that all vertices are reachable from the root vertex. i.e. we have a connected graph.


## Hint
- Hash table
- Depth first traversal


## Reference:
https://www.educative.io/m/clone-directed-graph