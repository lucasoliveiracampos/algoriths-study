import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.LinkedList;
//Started to build structure from problem at 20:51 stopped at new LinkedList 21:36 to live :)
//Came back around 11:00 and finished at 13:03!!!
//TOOOO MUCH TIME!! Must to practice a bit more to fix this concept into my mind!
public class CloneDirectedGraph {

    public static class Node {
        int value;
        List<Node> neighbors = new LinkedList<Node>();
        int counter = 0;

        public Node() {}

        public Node(int value) {
            this.value = value;
        }        

        public Node(int value, Node... neighbors) {
            this.value = value;
            this.neighbors = Arrays.asList(neighbors);
        }

        @Override
        public boolean equals(Object o) {
            if (o == this)
                return true;
            if (!(o instanceof Node))
                return false;
            Node node = (Node)o;
            return this.value == node.value;
        }

        @Override
        public int hashCode() {
            return 31 + this.value;
        }

        @Override
        public String toString() {
            return String.format("Node value %s with %s neighbors", this.value, this.neighbors.size());
        }

        public Node deepCopy() {
            Map<Node, Node> map = new HashMap<Node, Node>();
            return this.internalDeepCopy(map);
        }

        private Node internalDeepCopy(Map<Node, Node> map) {
            Node outputNode = new Node();
            outputNode.value = this.value;
            map.put(this, outputNode);
            System.out.println(String.format("Added value %s into root", outputNode.value));
            for (Node neighbor: this.neighbors) {
                Node copiedNeighbor = map.get(neighbor);
                if (copiedNeighbor == null && neighbor != null) {
                    copiedNeighbor = neighbor.internalDeepCopy(map);
                }
                outputNode.neighbors.add(copiedNeighbor);
                System.out.println(
                    String.format("Added value %s into Node %s",
                    neighbor.value, outputNode.value)
                );
            }
            return outputNode;
        }
    }


    public static void main(String[] args) {
        Node node0 = new Node(0);
        Node node2 = new Node(2, node0);
        Node node1 = new Node(1, node2);
        Node node3 = new Node(3, node2);
        Node node4 = new Node(4, node3, node1);
        node0.neighbors = Arrays.asList( new Node[]{node2, node3, node4});

        Node newNode = node0.deepCopy();
        System.out.println(newNode);
        for (Node n : newNode.neighbors) {
            System.out.println("n = " + n);
            for (Node n2 : n.neighbors) {
                System.out.println("n2 = " + n2);
                for (Node n3 : n2.neighbors) {
                    System.out.println("n3 = " + n3);
                }
            }
        }
    }
}
